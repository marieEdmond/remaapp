module.exports = {
  purge:["./src/**/*.vue"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        gris:"#667085",
        greenCompact:"#16B853",
        greenFlou:"#F6FFFB",
        blueCompact:"#081F50",
        violet:" #6941C6",
        rouge:"#FA7575"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
