import { createWebHistory, createRouter } from 'vue-router';
import pathLoader from '../plugins/loader'
const routes = [
    {
        path: '/',
        name: 'Home',
        component: pathLoader('home'),
    },
    {
        path: '/signup',
        name: 'Signup',
        component: pathLoader('register'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
