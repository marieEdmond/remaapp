/**
 * *
This is path loader
Just for no always import page
 **
 */
export default (path, file = false) => {
    return () => {
        return import(`../views/${path || ''}${path ? '/' : ''}${file || 'index'}.vue`)
    }
}
